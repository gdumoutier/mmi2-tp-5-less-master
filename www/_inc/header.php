<?php
include_once('_inc/routes.php');
include_once('_inc/functions.php') ;
?>

<!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>MMI2 TP1</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!--<base href="http://127.0.0.1/mmi2-tp-4/www/" />-->

	<link rel="stylesheet" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Roboto+Mono" rel="stylesheet">

	<script src="https://use.fontawesome.com/601a2e39a6.js"></script>
</head>
<body>
<div class="layout remodal-bg">

	<header role="banner">
		<div class="wrapper">
			<p class="logo">
				<a href="<?= $route['accueil'] ?>">
					TP LESS & CSS
				</a>
			</p>
		</div>
	</header>
	<main>
		<div class="wrapper">

<?php
if(isset($_POST)){
	if(!empty($_POST['mail']) || !empty($_POST['subject']) || !empty($_POST['message'])){
		extract($_POST);
		echo "<p class='alert alert-success'>message envoyé !</p>";
		$messageHTML = "<p>Message de : <strong>$mail</strong></p><p>$message</p>";
		sendMail($_POST['mail'],"destinataire@example.com",$subject,$messageHTML,true);
	}
}
?>