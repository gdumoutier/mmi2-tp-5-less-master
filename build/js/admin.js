//@prepros-prepend imports/jquery.js
//@prepros-prepend imports/helpers.js
//@prepros-prepend imports/bootstrap.js
//@prepros-prepend imports/sb-admin-2.js
//@prepros-prepend imports/gmaps.js

(function($){

	$('#preview').on('submit',function(e){
		e.preventDefault();
		//var request = $(this).serializeArray();
		//console.log(request);
		//console.log(request[0].name);

		//var request = $(this).serializeObject();

		console.log($(this).attr('action'));
		$.ajax({
			method: "get",
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(json){
				console.log(json);
				var data = $.parseJSON(json);
				$('.panel-heading').html(data.title+' | '+data.sum);
				$('.panel-body').html(data.content);
				$('.panel-footer').html(data.type);
			}
		});


	});

	if($('#map').length > 0){
		var map = new GMaps({
			div: '#map-display',
			lat: -12.043333,
			lng: -77.028333
		});

		$('#map').on('submit',function(e){
			e.preventDefault();
			GMaps.geocode({
				address: $("#address").val(),
				callback: function(results, status) {
					if (status == 'OK') {
						var latlng = results[0].geometry.location;
						map.setCenter(latlng.lat(), latlng.lng());
						map.addMarker({
							lat: latlng.lat(),
							lng: latlng.lng()
						});
					}else{
						alert('erreur');
					}
				}
			});
		});
	}

})(jQuery);